package com.zuitt;

import java.util.Date;

public class Course {
    private String name;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;

    private Date endDate;
    private String instructor;

    public Course(){

    }
    public Course(String name, String description,int seats,double fee,Date startDate,Date endDate,String instructor){
        this.name = name;
        this.description =description;
        this.seats =seats;
        this.fee =fee;
        this.startDate =startDate;
        this.endDate =endDate;
        this.instructor =instructor;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getDescription(){
        return this.description;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public int getSeats(){
        return this.seats;
    }
    public void setSeats(int seats){
        this.seats = seats;
    }
    public Double getFee(){
        return this.fee;
    }
    public void setFee(Double fee){
        this.fee = fee;
    }
    public Date getStartDate(){
        return this.startDate;
    }
    public void setStartDate(Date startDate){
        this.startDate = startDate;
    }

    public Date getEndDate(){
        return this.endDate;
    }
    public void setEndDate(Date endDate){
        this.endDate = endDate;
    }

    public String getInstructor(){
        return this.instructor;
    }
    public void setInstructor(String instructor){
        this.instructor = instructor;
    }



}
