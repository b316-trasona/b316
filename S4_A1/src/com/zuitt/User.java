package com.zuitt;

public class User {
    private String name;
    private int age;
    private String email;
    private String address;
    public User(){

    }
    public User(String name, int age,String email,String address){
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public int getAge(){
        return this.age;
    }
    public void setAge(int name){
        this.age = age;
    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public String getAddress(){
        return this.address;
    }
    public void setAddress(String address){
        this.address = address;
    }
}
