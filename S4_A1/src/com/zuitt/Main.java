package com.zuitt;
import java.text.ParseException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Terrence Gaffud",25,"tgaff@mail.com","Quezon City");
        Course course1 = new Course();
        course1.setName("MAC004");
        course1.setDescription("An introduction to Java for career-shifters");
        course1.setSeats(30);
        course1.setFee(20000.00);
        String startDate = "1-06-2023";
        String endDate = "1-07-2023";
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");

        //Date date = formatter1.parse(startDate);
        Date yeah = new Date();
        Date yeah2 = new Date();
        try {
            yeah = formatter1.parse(startDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        try {
            yeah2 = formatter2.parse(endDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }




        course1.setStartDate(yeah);
        course1.setEndDate(yeah2);
        course1.setInstructor("Terrence Gaffud");
        System.out.println("Hi! I'm "+user1.getName()+". I'm "+user1.getAge()+" years old. You can reach me my email: "+user1.getEmail()+". When I'm off work, I can be found at my house in "+user1.getAddress()+".");
        System.out.println("Welcome to the course "+course1.getName()+". This course can be described as "+course1.getDescription()+". Your instructor for this course is Sir "+course1.getInstructor()+". Enjoy");



    }
}
