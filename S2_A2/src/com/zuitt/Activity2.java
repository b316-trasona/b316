package com.zuitt;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class Activity2 {
    public static void main(String[] args) {
        int[] prime = new int[5];
        prime[0] = 2;
        prime[1] = 3;
        prime[2] = 5;
        prime[3] = 7;
        prime[4] = 11;
        System.out.println("the first prime number is: "+prime[0]);

        ArrayList<String> friends = new ArrayList<String>();

        //Adding elements to Array List
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are : "+friends);

        HashMap<String,Integer> inventory = new HashMap<String,Integer>();

        //adding elements into the hashmap
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of: "+inventory);





    }
}
