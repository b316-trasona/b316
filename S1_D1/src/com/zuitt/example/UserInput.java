package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);//Create a scanner object
        System.out.println("enter a username");

        String userName = myObj.nextLine();//Reads user input
        System.out.println("username is "+userName);
    }
}
