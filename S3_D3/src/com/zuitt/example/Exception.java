package com.zuitt.example;

import java.util.Scanner;

public class Exception {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //nextInt()-expects integer, nextDouble()-expects a Double, nexLine()- gets the entire line as String
        System.out.println("Input a number:");
        int num = 0;//input.nextInt();
        try{
            num = input.nextInt();

        }catch(java.lang.Exception e){
            //java.lang.Exception because nagconflict sa class name
            System.out.println("Invalid Input");
            e.printStackTrace();
        }

        System.out.println("you have entered: "+num);
    }
}
