package com.zuitt.sample;

public class Dog extends Animal {
    //extends keyword allows us to have this class inherit the attributes and methods of the animal class
    //parent class is the class where we inherit from
    //child class/subclass a class taht inherits from a parent
    //No, Sub-class cannot inherit from multiple parents
    //Instead Subclass can mutiple inheret from what we call interfaces

    private String dogBreed;
    public Dog(){
        super();//super is areference to the parent cl
        this.dogBreed = "chihuahua";
    }
    public Dog(String name,String color, String breed){
        super(name,color);
        this.dogBreed = breed;
    }

    public String getDogBreed(){
        return this.dogBreed;
    }
    public void setDogBreed(String dogBreed){
        this.dogBreed = dogBreed;
    }

    public void greet(){
        super.call();
        System.out.println("Bark");
    }
}
