package com.zuitt.sample;

public class Car {
    //Properties/Attributes - the characteristics of the object the class will create
    //Constructor - method to create tthe object and instantiate with its initial value
    //Getters and setters = methods that get values of an object property or set them
    //method - actions taht an object can perform or do
    //public access
    //private
    private String make;

    private String brand;

    private int price;

    private Driver carDriver;

    //Constructor is a method to construct the initial value of an instance
    public Car(){
        this.carDriver = new Driver();
    }
    public Car(String make,String brand,int price,Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver =driver;
    }

    //getter and setter for our properties
    //Getters return a value so therefore we must add the data type of the value returned
    public String getMake(){
        return this.make;
    }

    public void setMake(String makeParams){
        this.make = makeParams;
    }

    public String getBrand(){
        return this.brand;
    }

    public void setBrand(String brandParams){
        this.brand = brandParams;
    }

    public int getPrice(){
        return this.price;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }

    //Classes have relationship
    /*
    * Composition allows modelling objects to be made up of other objects. Classes can have instances of other objects
    *
    *
    * */
    public Driver getCarDriver(){
        return this.carDriver;

    }

    public void setCarDriver(Driver carDriver){
        this.carDriver = carDriver;
    }

    //custom method to retrive cardrivers name
    public String getCarDriverName(){
        return this.carDriver.getName();
    }
    public void start(){
        System.out.println("Vroom Vroom");
    }
}
