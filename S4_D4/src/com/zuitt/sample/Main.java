package com.zuitt.sample;

public class Main {

    public static void main(String[] args) {
//        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//
//        Car car2 = new Car();
//        car2.make = "Evo";
//        car2.brand = "Mitsubishi";
//        car2.price = 300000;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//
//        Car car3 = new Car();
//        car3.make = "Avanza";
//        car3.brand = "Toyota";
//        car3.price = 400000;
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);
        //Create Two new instances of the Car class
        Car car1 = new Car();

        Car car2 = new Car();
        Car car3 = new Car();

        Driver driver1 = new Driver("alejandro",25);
 //       System.out.println(driver1.name); //only works if name is public
        car1.start();
        car2.start();
        car3.start();

        //property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        //System.out.println(car3.getMake());
        car2.setMake("Innova");
        System.out.println(car2.getMake());

        //carDriver getter
        System.out.println(car1.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio",21);
        car1.setCarDriver(newDriver);

        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());
        /*
        * Mini activity
        * Create a new class called animal
        *
        *
        *
        *
        *
        * */

        Animal ani1 = new Animal("horse","white");
        System.out.println(ani1.getName());
        System.out.println(ani1.getColor());
        ani1.call();

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());

        Dog dog2 = new Dog("Mike","Brown","Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();

    }
}
