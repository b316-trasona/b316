package com.zuitt.example;

public class Person implements Actions,Greetings{//the implements keyword is neede when using interfaces
    public void sleep(){
        System.out.println("zzzz...");

    }
    public void run(){
        System.out.println("running on the road");
    }

    public void walk(){
        System.out.println("walking on the street");
    }
    public void swim(){
        System.out.println("Swimming on the pool");
    }

    public void morning(){
        System.out.println("Good Morning");
    }

    public void holiday(){
        System.out.println("happy holidays");
    }

    /*Mini activity
    * create two new actions
    *
    * */
}
