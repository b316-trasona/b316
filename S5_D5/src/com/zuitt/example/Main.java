package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("hello");

        Person person1 = new Person();
        person1.run();
        person1.sleep();
        person1.walk();
        person1.swim();

        StaticPoly staticPoly = new StaticPoly();
        System.out.println(staticPoly.addition(1,5));
        System.out.println(staticPoly.addition(1,5,10));
        System.out.println(staticPoly.addition(15.6,15.6));

        Parent parent1 = new Parent("John",35);
        parent1.greet();
        parent1.greet("John","morning");
        parent1.speak();

        Child newChild = new Child();
        newChild.speak();


    }
}
