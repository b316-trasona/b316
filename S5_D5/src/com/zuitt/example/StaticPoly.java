package com.zuitt.example;

public class StaticPoly {
    //Ststic Polymorphism - the ability to have multiple mrthods of the same name but changes form based on the number of arguments or types of arguments

    public int addition(int a,int b){
        return a + b;
    }
    //Overload by changing the number of arguments
    public int addition(int a,int b, int c){
        return a+b+c;
    }
    //Overloading by changing the type
    public double addition(double a,double b){
        return a + b;
    }
}
