package com.zuitt.example;

public interface Greetings {
    public void morning();
    public void holiday();
}
