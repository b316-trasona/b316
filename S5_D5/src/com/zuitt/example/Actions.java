package com.zuitt.example;

public interface Actions {
    public void sleep();
    //interfaces are like blueprints
    public void run();

    public void walk();
    public void swim();


}
