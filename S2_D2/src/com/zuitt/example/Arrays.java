package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class Arrays {

    public static void main(String[] args) {
        //Declaration
        int[] intArray = new int[5];
        //the [] indicates this int data type should hold multiple int values
        //"new" is a keyword used in non-primitive datatype to tell java to create said variable
        // the integer value inside the bracket on this side of the statement indicates the amount of integer values the array can hold

        //Declaration with initialization
        int[] intArray2 ={100,200,300,400,500};

        //Multidimentional array
        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //System.out.println(classroom.toString());
        ArrayList<String> students = new ArrayList<String>();

        //Adding elements to Array List
        students.add("John");
        students.add("Paul");

        //Accessing elements of the ArrayList
        students.get(0);

        //Accessing
        students.set(1,"george");

        //Removing
        students.remove(1);

        //Remove all
        students.clear();

        System.out.println(students.size());

        //Hashmaps;
        HashMap<String,String> job_position = new HashMap<>();

        //adding elements into the hashmap
        job_position.put("Brandon","Student");
        job_position.put("Alice","Dreamer");

        //Accessing elements of the Hashmap
        job_position.get("Alice");
        System.out.println(job_position.get("Alice"));

        //Removing elements in Hashmap
        job_position.remove("Brandon");
        System.out.println(job_position);

        //Getting the key of the elements of hashmaps
        System.out.println(job_position.keySet());

        //Operators allow us to manipulate the values that we store in variables.They represent logical and arithmetic operations
//       /*
//       types of operators
        //arithmetic: +,-,*,/,%
        //comparison: >, <, >=,=>,==,!=
        //Logical: &&, ||, !
        //Assignment: =


//       */


    }


}
