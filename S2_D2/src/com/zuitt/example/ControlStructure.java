package com.zuitt.example;

public class ControlStructure {

    public static void main(String[] args) {
        //if statements
        int num1 = 10;
        int num2 = 20;
        if(num1 > 5){
            System.out.println("Num1 is greater than 5");

        }

        if(num2 > 100){
            System.out.println("num2 is greater that 100");

        } else {
            System.out.println("num2 is less than 100");
        }

        //Short Circuiting
        int x = 15;
        int y = 0;
//        if(y>0 || x/y == 0){
//            System.out.println("Result is"+ x/y);
//        }
        //switch Cases
        int directionValue = 4;
        switch (directionValue){
            case 1://A case block within a switch statement. this represents a single case or a single possible value for its statement
                System.out.println("North");
                break;
                // the break keyword tell that this specific case block has finished
            case 2:
                System.out.println("south");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("invalid");
        }
    }
}
