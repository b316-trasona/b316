package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        Scanner userScanner = new Scanner(System.in);
        System.out.println("First Name:");

        String firstName = userScanner.nextLine();
        System.out.println("Last Name:");
        String lastName = userScanner.nextLine();
        System.out.println("First Subject Grade:");
        String firstSubjectGrade = userScanner.nextLine();
        double convertedFirstSubjectGrade = Double.parseDouble(firstSubjectGrade);

        System.out.println("Second Subject Grade:");
        String secondSubjectGrade = userScanner.nextLine();
        double convertedSecondSubjectGrade = Double.parseDouble(secondSubjectGrade);

        System.out.println("Third Subject Grade:");
        String thirdSubjectGrade = userScanner.nextLine();
        double convertedThirdSubjectGrade = Double.parseDouble(thirdSubjectGrade);

        int average = (int)(convertedFirstSubjectGrade + convertedSecondSubjectGrade + convertedThirdSubjectGrade)/3;

        System.out.println("Good day, "+firstName+" "+lastName);
        System.out.println("Your grade average is: "+average);



    }
}
