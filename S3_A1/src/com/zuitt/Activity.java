package com.zuitt;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");
        int num = 0;
        int answer = 1;
        int counter = 1;
        String err = "";
        try{
            num = in.nextInt();
        }catch(Exception e){
            System.out.println("Invalid input"+e);
            err = "err";
            e.printStackTrace();
        }

        if(num < 0){
            if(err == ""){
                System.out.println("Invalid input");
            }
            //System.out.println("Invalid input");
        }
        //while loop--------------------------------
        while(counter <= num){
            answer *= counter;
            counter++;
        }

        if(num<0){
            System.out.println("Can't compute factorial of "+num);
        } else {
            System.out.println("The factorial of "+num+" is "+answer);
        }

        //for loop--------------------------
        answer = 1;
        counter = 1;
        for(;counter <= num;counter++){
            answer *= counter;
        }

        if(num<0){
            System.out.println("Can't compute factorial of "+num);
        } else {
            System.out.println("The factorial of "+num+" is "+answer);
        }



    }
}
