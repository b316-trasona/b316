package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook(){

    }
    public Phonebook(Contact contact){
        this.contacts.add(contact);

    }

    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }
}
