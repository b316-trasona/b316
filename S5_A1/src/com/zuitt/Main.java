package com.zuitt;

import java.util.ArrayList;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe","+639152468596","home is Quezon city");
        Contact contact2 = new Contact("John Doe","+639228547963","office is Makati City");
        Contact contact3 = new Contact("Jane Doe","+639162148573","home is Caloocan City");
        Contact contact4 = new Contact("Jane Doe","+639173698541","office is Pasay City");
       // System.out.println(phonebook.getContacts());
        //String conName = "none";
        ArrayList<String> doneNames = new ArrayList<>();

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        phonebook.setContacts(contact3);
        phonebook.setContacts(contact4);
        if(phonebook.getContacts().size() == 0){
            System.out.println("Phonebook is empty");
        } else {
            for (Contact i: phonebook.getContacts()){
                if (doneNames.size() == 0 || doneNames.contains(i.getName()) == false){
                    System.out.println(i.getName());
                    System.out.println("---------------");
                    System.out.println(i.getName()+" has the following registered numbers:");
                    for(Contact j: phonebook.getContacts()){
                        if(Objects.equals(j.getName(), i.getName())){
                            System.out.println(j.getContactNumber());
                        }
                    }
                    System.out.println("---------------");
                    System.out.println(i.getName()+" has the following registered addresses:");
                    for(Contact j: phonebook.getContacts()){
                        if(Objects.equals(j.getName(), i.getName())){
                            System.out.println("my "+j.getAddress());
                        }
                    }
                    System.out.println("==========================");

                    doneNames.add(i.getName());



                    //conName = i.getName();
                }
            }

        }

    }
}
