package com.zuitt;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        Scanner myScan = new Scanner(System.in);
        System.out.println("Input year to be checked for leap year.");

        String year = myScan.nextLine();
        int intYear = Integer.parseInt(year);
        if(((intYear % 4 == 0) && (intYear % 100!= 0)) || (intYear % 400 == 0)){
            System.out.println(intYear+" is a leap year");

        } else {
            System.out.println(intYear+" is NOT a leap year");
        }



    }
}
